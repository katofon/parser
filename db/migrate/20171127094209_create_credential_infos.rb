class CreateCredentialInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :credential_infos do |t|
      t.string :name
      t.string :city
      t.string :credential
      t.integer :earned
      t.string :status

      t.timestamps
    end
  end
end
